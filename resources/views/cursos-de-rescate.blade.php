@extends('layouts.master')


@section('content')
<div class="header_title pade_bg1 d-flex justify_content_center align_items_center bg_overlay1">
    <div class=" text-center">
                <h1 class="page_title text-center">Cursos de rescate</h1>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 textwhite">
            <p>En Paddle Patagonia trabajamos con el programa de certificación de <strong>Boreal River Rescue</strong>, brindando exelentes cursos de rescate en aguas blancas para guías de río, instructores de navegación, kayakistas, personas que trabajan cerca o dentro de ríos y profesionales de rescate.

                Nuestros programas son intensivos, prácticos y están bien organizados. Actualizamos constantemente el temario de los cursos para que contenga siempre información pertinente y útil. Los cursos incluyen práctica de habilidades, simulacros de rescate, debates y revisiones prácticas.

                Durante más de una década, los instructores de Boreal River Rescue han establecido los estándares de excelencia internacionales en materia de entrenamiento para rescates en aguas blancas. Poseemos experiencia en rescates reales y en la instrucción de éstas habilidades en aguas blancas en una amplia gama de ambientes, desde enseñanza a remadores en excursiones de ríos remotos, hasta enseñanza a bomberos en inundaciones de canales urbanos. Nuestros instructores cuentan con una basta experiencia, además de ser profesionales y muy divertidos.</p>
                <p><strong>Organiza un curso para tu compañía, club, grupo o escuela.</strong></p>
                <p>Realiza un curso de Boreal River Rescue en el sitio donde te encuentres. Nosotros pondremos a disposición los instructores, el equipamiento y un curso de excelente calidad. Tú organiza el grupo. Puedes hacerlo para tu propio equipo de trabajo, miembros de un grupo, o abierto para el público en general. Trabajaremos contigo para asegurarnos de que el curso se lleve a cabo a la perfección y te ayudaremos a promocionarlo. Envíanos un e-mail a: <strong> info@paddlepatagonia.com</strong> para comenzar.</p>

                <section class="two">
                <div id="accordion" class="panel-group">



                    <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panel11" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Rescate en Aguas Blancas (WWR) – dos días (niveles I & II)</a>
                          </h4>
                        </div>
                        <div id="panel11" class="panel-collapse collapse in" >
                        <div class="panel-body">
                            <p>El curso Rescate en Aguas Blancas (WWR) consta de dos días de aprendizaje y práctica dentro y en los alrededores del río. Obtén una base de las habilidades y conocimiento de rescate que necesitas para sentirte seguro al trabajar y/o divertirte en ríos de aguas blancas.</p>
                          </div>
                        </div>
                      </div>
                       <!--Panels-->
                    <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panel12" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Técnico de Rescate en Aguas Blancas (WRT) – cuatro días (niveles I, II & III)</a>
                          </h4>
                        </div>
                        <div id="panel12" class="panel-collapse collapse in" >
                        <div class="panel-body">
                            <p>Estándar internacional para guías de río, instructores de aguas blancas, kayakistas avanzados y técnicos de rescate. En este curso intensivo y práctico pondrás en práctica y perfeccionarás tus habilidades de rescate en los distintos ambientes del río. </p>
                          </div>
                        </div>
                      </div>
                       <!--Panels-->
                    <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panel13" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion">Técnico de Rescate en Aguas Blancas Puente (WRT-Puente) – dos días (nivel III)</a>
                          </h4>
                        </div>
                        <div id="panel13" class="panel-collapse collapse in" >
                        <div class="panel-body">
                            <p>Este curso es para quienes se hayan graduado de Rescate en Aguas Blancas (WWR) y deseen mejorar sus habilidades y certificarse en el nivel de Técnico de Rescate en Aguas Blancas (WRT). Para inscribirte, debes contar con una certificación vigente de WWR o una certificación similar de rescate en aguas blancas de otro proveedor, como la American Canoe Association, Rescue Canada, Rescue for River Runners o Rescue 3 International.</p>
                          </div>
                        </div>
                      </div>
                       <!--Panels-->
                    <div class="panel">
                        <div class="panel-heading">
                        <h4 class="panel-title">
                          <a href="#panel14" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"> Recertificación Técnico de Rescate en Aguas Blancas (WRT-Recertificación) – dos días (nivel III)</a>
                          </h4>
                        </div>
                        <div id="panel14" class="panel-collapse collapse in" >
                        <div class="panel-body">
                            <p>La renovación de la certificación Técnico de Rescate en Aguas Blancas (WRT) se realiza en dos días y está orientada a aquellas personas que tengan una certificación actual de WRT expedida por Boreal River Rescue, o una certificación equivalente de otro proveedor, como por ejemplo “R3-pro” de Rescue for River Runners o “Swiftwater Rescue Technician” (Técnico de rescate en aguas blancas) – Nivel 1(SRT-1) de Rescue 3 International, Rescue Canada o de la American Canoe Association. </p>
                          </div>
                        </div>
                      </div>
                       <!--Panels-->
                    </div>
                </section>

        </div>
    </div>
</div>






@endsection
