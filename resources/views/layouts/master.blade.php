<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>@yield('title')</title>
	<link rel="icon" type="image/png" href="/assets/images/favicon.png">
    <!--Main style css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="/assets/css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<!--Responsive style css-->
	<link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="/assets/css/animate.css">

    @yield('css')
</head>
<body>
	<!-- main navigation mobile -->
	<div class="main_nav_mobile" id="topNavMobile">
		<div class="main_nav_mobile_inner">
			<div class="navbar_nav_over_wrapp">
				<ul class="navbar-nav">
					<li class="has-child">
						<a href="#">CURSOS KAYAK</a>
						<ul class="sub-menu">
							<li><a href="#">APRENDE A REMAR</a></li>
							<li><a href="#">MEJORA TU TÉCNICA</a></li>


						</ul>
					</li>
					<li><a href="#">CURSOS RAFTING</a></li>
					<li><a href="#"></a></li>
					<li class="has-child">
						<a href="/cursos-de-rescate">CURSOS DE RESCATE</a>
						<ul class="sub-menu">
							<li><a href="#">WWR-2 días</a></li>
							<li><a href="#">WRT-4 días</a></li>

						</ul>
					</li>

					<li><a href="/nosotros">NOSOTROS</a></li>

				</ul>
			</div>
			<button class="close_mobile_menu" type="button" data-target="#topNavMobile">
				<span class="navbar-toggler-icon"><i class="fas fa-times"></i></span>
			</button>
		</div>
		<div class="cover_mobile_menu" data-target="#topNavMobile"></div>
	</div>
    <!-- END main navigation mobile -->
    	<!-- main header -->
	<header class="main_header fixed_header transparen_bg_head">
		<div class="container clearfix">
			<div class="logo_head">
				<a href="/"><img src="/assets/images/21.png" alt=""></a>
			</div>
			<div class="navbar-expand-lg nav_btn_toggle">
				<button class="navbar-toggler open_mobile_menu" type="button" data-target="#topNavMobile">
					<span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
				</button>
			</div>
			<nav class="top_nav_links navbar navbar-expand-lg">
				<div class="collapse navbar-collapse" id="topNav">
					<ul class="navbar-nav mr-auto">


					<li class="has-child">
						<a href="#">CURSOS KAYAK</a>
						<ul class="sub-menu">
							<li><a href="#">APRENDE A REMAR</a></li>
							<li><a href="#">MEJORA TU TÉCNICA</a></li>


						</ul>
					</li>
					<li><a href="#">CURSOS RAFTING</a></li>

					<li class="has-child">
						<a href="/cursos-de-rescate">CURSOS DE RESCATE</a>
						<ul class="sub-menu">
							<li><a href="#">WWR-2 días</a></li>
							<li><a href="#">WRT-4 días</a></li>
						</ul>
                    </li>
                    <li><a href="/nosotros">NOSOTROS</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header><!--END main header -->
	<!-- main_big_carousel -->
@yield('content')

	<footer class="main_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-3 footer_block">
					<a href="#"><img src="assets/images/rioslogo.png" alt=""></a>
					<!--p class="copy">
						Copyrights 2018,<br>
						HTML Template <br>
						all rights reserved
					</p-->
				</div>
				<div class="col-md-6 col-lg-3 footer_block">
						<h6 class="footer_block_title">About us</h6>
						<p>Lorem Ipsum is simply dummy<br>
						text of the printing and<br>
						typesetting industry.</p>
				</div>
				<div class="col-md-6 col-lg-3 footer_block">
						<h6 class="footer_block_title">Contact us</h6>
						<p>1600 Amphitheatre Parkway<br>
						Mountain View, CA 94043<br>
						Phone: +1 650-253-0000</p>
				</div>
				<div class="col-md-6 col-lg-3 footer_block">
					<div class="social_wg">
						<h6 class="footer_block_title">Socials</h6>
						<p><a href="#"><i class="fab fa-twitter-square"></i></a>
						<a href="#"><i class="fab fa-facebook-square"></i></a>
						<a href="#"><i class="fab fa-youtube"></i></a>
						<a href="#"><i class="fab fa-instagram"></i></a></p>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- END main_footer -->

	<!-- JS -->
	<!--jQuery 1.12.4 google link-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!--Bootstrap 4.1.0-->
	<script src="/assets/libs/bootstrap-4.1.0/bootstrap.min.js"></script>
	<!--shuffle-->
	<script src="/assets/libs/shuffle/shuffle.min.js"></script>
	<!--jquery.magnific-popup-->
	<script src="/assets/libs/jquery.magnific-popup/jquery.magnific-popup.min.js"></script>
	<!--OwlCarousel2-2.3.4-->
	<script src="/assets/libs/owlcarousel2-2.3.4/owl.carousel.min.js"></script>
	<!--Custom js-->
    <script src="/assets/js/custom.js"></script>
    @yield('scripts')
</body>
</html>
