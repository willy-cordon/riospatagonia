@extends('layouts.master')

@section('title')
    Nosotros
@endsection

@section('content')
@component('components.principal')
@endcomponent

<div class="container">
    <div class="row">
        <div class="col-md-12 textwhite">
           <p>Somos una pequeña compañía que se dedica al turismo de aventura, especializada en excursiones en canoa y kayak en aguas blancas, y en vacaciones de turismo aventura. Nuestra base está situada en la Patagonia argentina, a las afueras de la pequeña ciudad de El Bolsón. Nuestros paseos fueron diseñados para combinar una experiencia genuina de viaje por la Patagonia, con actividades divertidas y de aventura que a nuestros huéspedes tanto les gustan!</p>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-12">
        <hr>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 textwhite">
            <img id="imgenf"src="/assets/images/julian.jpg" alt="">
            <p>Julián creció en la Patagonia argentina y desde muy chico disfrutó de sus ríos y montañas. Sin embargo, su verdadera pasión por las aguas blancas surgió en su adolescencia, luego de su primera experiencia en kayak en el río Futaleufú (Chile). Viviendo a sólo 3 horas del Futaleufú, fue inevitable que Julián se convirtiera en guía de ese desafiante río. Desde entonces, ha trabajado como guía en Canadá, los Estados Unidos, México, Costa Rica, Zambia y Argentina. Como instructor, Julián es muy talentoso y paciente, además de ser un habilidoso guía de rafting y un excelente kayakista y canoísta. Está certificado en Primeros Auxilios, es técnico instructor de rescate en aguas blancas para Boreal River Rescue y es instructor de kayak y canoa para Paddle Canada.
                Naturalmente, en sus tiempos libres sale a remar y siempre está buscando nuevos “primeros descensos” en ríos de la Patagonia, aunque también disfruta trabajar sobre tierra firme. Actualmente, él y Katie están construyendo su primer hogar aquí en la Patagonia.</p>
        </div>
        <div class="col-md-6 textwhite">
            <img id="imgenf"src="/assets/images/fidel.jpg" alt="">
            <p>Fidel creció en un pequeño pueblo en Argentina llamado Trevelín, y desde hace muchos años es instructor de kayak en aguas blancas y guía de  rafting en Chile, Argentina y Canadá. Comenzó siendo un nadador de competición, pero ahora dedica (casi) todo su tiempo libre a andar en kayak. Estamos orgullosos de contarles que Fidel consiguió el 2° lugar en el campeonato Panamericano de Kayak Estilo Libre de 2012. Fidel es además un excelente cocinero y un experto en la pesca con mosca, y aporta una gran energía a todos los viajes de Paddle Patagonia.</p>
        </div>
    </div>
</div>

@endsection

