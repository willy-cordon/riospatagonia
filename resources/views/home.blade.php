@extends('layouts.master')

@section('content')

<div class="main_big_slider" id="main_slider1">
    <div class="owl-carousel main_b_slider">
        <!--Item big slider-->
        <div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_1 bg_overlay1">

                <div class="text-center">
                    <h1 class="slider_big_title">
                        Increibles Escenarios
                    </h1>

                    <div class="slider_sub_title">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt</p>
                    </div>
                    <div class="text-center">
                        <a href="#our_photo" class="sv_btn sv_btn_default easescroll">ver mas</a>
                    </div>
                </div>

        </div><!--END Item big slider-->
        <!--Item big slider-->
        <div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_2 bg_overlay1">

                <div class="text-center">
                    <h1 class="slider_big_title">
                        Aguas blangas
                    </h1>
                    <div class="slider_sub_title">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
                    </div>
                    <div class="text-center">
                        <a href="#our_photo" class="sv_btn sv_btn_default easescroll">ver mas</a>
                    </div>
                </div>

        </div><!--END Item big slider-->
        <!--Item big slider-->
        <div class="d-flex bg_img justify_content_center align_items_center item item_mainslide_3 bg_overlay1">

                <div class="text-center">
                    <h1 class="slider_big_title">
                        Campamento y diversión
                    </h1>
                    <div class="slider_sub_title">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam noempor invidunt<br> ut labore et dolore magna aliquyam erat</p>
                    </div>
                    <div class="text-center">
                        <a href="#our_photo" class="sv_btn sv_btn_default easescroll">ver mas</a>
                    </div>
                </div>

        </div><!--END Item big slider-->
    </div>
</div><!--END main_big_carousel -->
<!-- advatnages_list -->
<div class="advatnages_list light_area section_padding_100_100">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 single_cool_fact">
                <div class="text-center">
                    <div class="cool_fact_icon_img">
                        <img src="assets/images/icon-img/cukl.png" alt="">
                    </div>
                    <h4 class="cool_fact_name">Cycling</h4>
                    <p>
                        Lorem Ipsum is simply dummy<br> text of the printing and typesetting industry simply.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 single_cool_fact">
                <div class="text-center">
                    <div class="cool_fact_icon_img">
                        <img src="assets/images/icon-img/kayaking.png" alt="">
                    </div>
                    <h4 class="cool_fact_name">Kayaking</h4>
                    <p>
                        Lorem Ipsum is simply dummy<br> text of the printing and typesetting industry simply.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 single_cool_fact">
                <div class="text-center">
                    <div class="cool_fact_icon_img">
                        <img src="assets/images/icon-img/camping.png" alt="">
                    </div>
                    <h4 class="cool_fact_name">Camping</h4>
                    <p>
                        Lorem Ipsum is simply dummy<br> text of the printing and typesetting industry simply.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 single_cool_fact">
                <div class="text-center">
                    <div class="cool_fact_icon_img">
                        <img src="assets/images/icon-img/fishing.png" alt="">
                    </div>
                    <h4 class="cool_fact_name">Fishing</h4>
                    <p>
                        Lorem Ipsum is simply dummy<br> text of the printing and typesetting industry simply.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div><!--END advatnages_list -->

@endsection
