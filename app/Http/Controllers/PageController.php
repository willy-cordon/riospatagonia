<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        return view('home');
    }

    public function nosotros(){
        return view('nosotros');
    }

    public function cursos(){
        return view('cursos-de-rescate');
    }


}
